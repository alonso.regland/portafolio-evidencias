
package cuentabancariamain;

import javax.swing.JOptionPane;

public class CuentaBancaria {
   
    private String clave;
    private String nombre;
    private float monto;
    
    public CuentaBancaria (){
        clave = Entrada.leerString("Cual es la clave de la cuenta:");
        nombre = Entrada.leerString("Cual es el nombre del propietario de la cuenta:");
        monto = Entrada.Flotante("Cual es el monto que tienes:");
    }
    
    public CuentaBancaria (String clave, String nom){
        clave = this.clave;
        nom = this.nombre;
        
        monto = Entrada.Flotante("Cual es el monto que tienes:");
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public String getClave() {
        return clave;
    }

    public String getNombre() {
        return nombre;
    }

    public float getMonto() {
        return monto;
    }
    
    public float depositar (float cantidad){
        
        cantidad = Entrada.Flotante("Que cantidad se depositara a ala cuenta");
        if (cantidad < 0 ){
              JOptionPane.showMessageDialog(null,"No se pueden depositvar cantidades negativas");
        }else{
             this.monto = monto + cantidad; 
        }
      
        
        return this.monto;
    }
    
    public float retirar (float cantidad){
        
        
        
        if (cantidad > monto ){
             JOptionPane.showMessageDialog(null,"no se puede retirar una cantidad mayor a la del monto");
           
        }else{
            if (cantidad <0){
                JOptionPane.showMessageDialog(null,"No se puede retirar valores negativos");
            }else{
                  JOptionPane.showMessageDialog(null,"Retirando dinero de la cuenta!!!");
            }
           
        }
        
        return cantidad;
    }
    
    public void verdatos(){
        JOptionPane.showMessageDialog(null,"calve:" + clave);
        JOptionPane.showMessageDialog(null,"nombre:" + nombre);
        JOptionPane.showMessageDialog(null,"monto:" + monto);
    }
    
}
